#include <alsa/asoundlib.h>
#include <string>
#include <fstream>

namespace Alsa 
{
    snd_pcm_t* open(const std::string &device = "default");
    void close(snd_pcm_t* handle);
    void print(snd_pcm_t* const handle);
    void print(const snd_pcm_hw_params_t* const params);
    size_t get_buffer_size(const snd_pcm_hw_params_t* const params);
    /**
     * @brief Play from a stream
     * 
     * @param input_stream 
     * @param params 
     * @param pcm_handle 
     * @param seconds Seconds audio will play, negative number to play until end.
     */
    void play(
        std::ifstream& input_stream, 
        snd_pcm_hw_params_t* params, 
        snd_pcm_t* pcm_handle,
        const int seconds = -1
    );
    snd_pcm_hw_params_t* create_hw_params(
        snd_pcm_t* const pcm_handle, 
        int n_channels=2, unsigned int sample_rate=44100
    );
}