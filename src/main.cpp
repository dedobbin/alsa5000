#include "alsa.h"

int main (int argc, char **argv)
{
    const std::string input_path = argc > 1 ? argv[1] : "kperry.raw";

    snd_pcm_t *pcm_handle = Alsa::open();
    snd_pcm_hw_params_t* params = Alsa::create_hw_params(pcm_handle, 1, 44100);

    Alsa::print(pcm_handle);
    Alsa::print(params);

    std::ifstream file(input_path, std::ios::binary);
    if (!file.is_open()){
        printf("Failed to open file\n");
        return 1;
    }
    Alsa::play(file, params, pcm_handle);

    Alsa::close(pcm_handle);
    return 0;
}