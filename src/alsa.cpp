#include "alsa.h"

snd_pcm_t * Alsa::open(const std::string &device)
{
    snd_pcm_t *pcm_handle;
    unsigned int rc; 
    if (rc = snd_pcm_open(&pcm_handle, device.c_str(), SND_PCM_STREAM_PLAYBACK, 0) < 0) { \
        printf("ERROR: Can't open \"%s\" PCM device: %s\n", device.c_str(), snd_strerror(rc)); \
        return nullptr;
    } 
    return pcm_handle;
}

void Alsa::close(snd_pcm_t* handle)
{
    int rc;
    if ((rc=snd_pcm_drain(handle)) != 0) {
        printf("ERROR: Can't drain: %s\n", snd_strerror(rc));
    }
    
    if ((rc=snd_pcm_close(handle)) != 0) {
        printf("ERROR: Can't close: %s\n", snd_strerror(rc));
    }
}

void Alsa::print(snd_pcm_t* const handle)
{
    printf("PCM name: '%s'\n", snd_pcm_name(handle));
	printf("PCM state: %s\n", snd_pcm_state_name(snd_pcm_state(handle)));
}

snd_pcm_hw_params_t * Alsa::create_hw_params(snd_pcm_t* const pcm_handle, int n_channels, unsigned int sample_rate)
{
    snd_pcm_hw_params_t *params; 
    snd_pcm_hw_params_malloc(&params); 
    snd_pcm_hw_params_any(pcm_handle, params); 

    unsigned int rc;
    if (rc = snd_pcm_hw_params_set_access(pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED) < 0) {
        printf("ERROR: Can't set interleaved mode. %s\n", snd_strerror(rc));
    }

    if (rc = snd_pcm_hw_params_set_format(pcm_handle, params, SND_PCM_FORMAT_U8) < 0) {
		printf("ERROR: Can't set format. %s\n", snd_strerror(rc));
    }

	if (rc = snd_pcm_hw_params_set_channels(pcm_handle, params, n_channels) < 0) {
		printf("ERROR: Can't set channels number. %s\n", snd_strerror(rc));
    }

	if (rc = snd_pcm_hw_params_set_rate_near(pcm_handle, params, &sample_rate, 0) < 0) {
		printf("ERROR: Can't set rate. %s\n", snd_strerror(rc));
    }

    if ((rc = snd_pcm_hw_params(pcm_handle, params)) < 0){
		printf("ERROR: Can't set harware parameters. %s\n", snd_strerror(rc));
        return nullptr;
    }
    
    return params;
}

void Alsa::print(const snd_pcm_hw_params_t* const params)
{
    unsigned int tmp;
	snd_pcm_hw_params_get_channels(params, &tmp);
    printf("channels: %i ", tmp);

    snd_pcm_hw_params_get_rate(params, &tmp, 0);
	printf("rate: %d bps\n", tmp);

    snd_pcm_hw_params_get_rate(params, &tmp, 0);
	printf("rate: %d bps\n", tmp);

    // TODO: etc
}

size_t Alsa::get_buffer_size(const snd_pcm_hw_params_t* const params)
{
    // TODO: error checking
    snd_pcm_uframes_t frames;
	snd_pcm_hw_params_get_period_size(params, &frames, 0);

    int sample_size = 1;
    snd_pcm_format_t format;
    snd_pcm_hw_params_get_format(params, &format);
    printf("warning: get_buffer_size used hard coded sample size %d\n", sample_size);

    unsigned int n_channels;
	snd_pcm_hw_params_get_channels(params, &n_channels);

    // One frame is 1 sample of audio for all channels
    int buff_size = frames * n_channels * sample_size;
    return buff_size;
}

inline void snd_play(snd_pcm_t* pcm_handle, char* buff, const snd_pcm_uframes_t& frames){
    static unsigned int pcm = 0;
    if (pcm = snd_pcm_writei(pcm_handle, buff, frames) == -EPIPE) {
        printf("XRUN.\n");
        snd_pcm_prepare(pcm_handle);
    } else if (pcm < 0) {
        printf("ERROR. Can't write to PCM device. %s\n", snd_strerror(pcm));
    }
}

void Alsa::play(
    std::ifstream& input_stream, 
    snd_pcm_hw_params_t* params, 
    snd_pcm_t* pcm_handle,
    const int seconds
)
{
    int buffer_size = Alsa::get_buffer_size(params);
    char* buffer = (char*)malloc(buffer_size);
    unsigned int rate;
    snd_pcm_hw_params_get_rate(params, &rate, 0);

    snd_pcm_uframes_t frames;
	snd_pcm_hw_params_get_period_size(params, &frames, 0);

    // Note, this also check rate; seconds < 0, if so, just play to end
    for (int loops = (seconds * 1000000) / rate; seconds < 0 || loops > 0; loops--) {
        input_stream.read(buffer, buffer_size);
        if (!input_stream.good()) {
            printf("End of stream\n");
            break;
        }

        snd_play(pcm_handle, buffer, frames);
    }
    free(buffer);
}